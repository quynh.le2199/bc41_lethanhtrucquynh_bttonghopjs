let cau1 = () => {
  let contentHTML = '';
  let contentTr = '';
  for(let i = 1; i <= 100; i += 10) {
    for(let j = 1; j <= 10; j++) {
      contentTr = `<tr>
                        <td>${i}</td>
                        <td>${i+1}</td>
                        <td>${i+2}</td>
                        <td>${i+3}</td>
                        <td>${i+4}</td>
                        <td>${i+5}</td>
                        <td>${i+6}</td>
                        <td>${i+7}</td>
                        <td>${i+8}</td>
                        <td>${i+9}</td>
                      </tr>`
    }
    contentHTML += contentTr;
  }
  document.getElementById("result-c1").innerHTML = contentHTML;
}
cau1();

let arrA = [];
let themSoCau2 = () => {
  let soN = document.getElementById("n-cau2").value * 1;
  arrA.push(soN);
  document.getElementById("result-arr-c2").innerText = `${arrA.join(", ")}`;
}
let cau2 = () => {
  let check;
  let soThuongArr = [];
  let soNguyenToArr = [];
  for (let i = 0; i < arrA.length; i++) {
    check = true;
    if (arrA[i] < 2) {
      check = false;
    }
    else if (arrA[i] == 2) {
      check = true;
    }
    else if (arrA[i] % 2 == 0) {
      check = false;
    }
    else {
      for(let k = 3; k < Math.sqrt(arrA[i]); k += 2) {
        if (arrA[i] % k == 0) {
          check = false;
        }
      }
    }

    if (check) {
      soNguyenToArr.push(arrA[i]);
    }
    else {
      soThuongArr.push(arrA[i]);
    }
  }
  if (soThuongArr.length == arrA.length) {
    document.getElementById("result-c2").innerText = `Không có số nguyên tố trong mảng`;
  }
  else {
    document.getElementById("result-c2").innerText = `Các số nguyên tố có trong mảng: ${soNguyenToArr.join(", ")}`;
  }
}

let cau3 = () => {
  let soN = document.getElementById("n-cau3").value * 1;
  let S = 0;
  for (let i = 2; i <= soN; i++) {
    S += i;
  }
  S += 2 * soN;
  document.getElementById("result-c3").innerHTML = S;
}

let cau4 = () => {
  let soN = document.getElementById("n-cau4").value * 1;
  let uocCuaSoN = [];
  for (let i = 1; i <= soN; i++) {
    if (soN % i == 0) {
      uocCuaSoN.push(i);
    }
  }
  document.getElementById("result-c4").innerHTML = `Ước số của ${soN} là: ${uocCuaSoN.join(", ")}`;
}

let cau5 = () => {
  let soN = document.getElementById("n-cau5").value;
  let result = [];
  for (let i of soN) {
    result.unshift(i);
  }
  document.getElementById("result-c5").innerHTML = result.join("");
}

let cau6 = () => {
  let S = 0;
  for (let i = 1; i < 100; i++) {
    S += i;
    if (S >= 100) {
      document.getElementById("result-c6").innerHTML = `x = ${i-1}`;
      break
    }
  }
}

let cau7 = () => {
  let soN = document.getElementById("n-cau7").value * 1;
  let result = '';
  for (let i = 0; i <= 10; i++) {
    let tich = soN * i;
    let cuuChuong = `<div>${soN} x ${i} = ${tich}</div>`;
    result += cuuChuong;
  }
  document.getElementById("result-c7").innerHTML = result;
}

let cau8 = () => {
  var cards = ["4K", "KH", "5C", "KA", "QH", "KD", "2H", "10S", "AS", "7H", "9K", "10D"];
  var players = [ [], [], [], [] ];
  var player1 = players[0];
  var player2 = players[1];
  var player3 = players[2];
  var player4 = players[3];

  for (var i = 0; i < cards.length; i +=4 ) {
    player1.push(cards[i]);
  }
  for (var j = 1; j < cards.length; j +=4 ) {
    player2.push(cards[j]);
  }
  for (var k = 2; k < cards.length; k +=4 ) {
    player3.push(cards[k]);
  }
  for (var h = 3; h < cards.length; h +=4 ) {
    player4.push(cards[h]);
  }
  var result = '';
  for (var x = 0; x < players.length; x++) {
    var eachPlayer = `<div>Player ${x+1}: ${players[x]}</div>`;
    result += eachPlayer;
  }
  document.getElementById("result-c8").innerHTML = result;
}

let cau9 = () => {
  let m = document.getElementById("tongSoCon").value * 1;
  let n = document.getElementById("tongSoChan").value * 1;
  let x; // số con gà
  let y;  // số con chó

  y = (n/2) - m;
  x = m - y;
  
  document.getElementById("result-c9").innerHTML = `<div>Số gà: ${x} con</div>
                                                    <div>Số chó: ${y} con</div>`
}

let cau10 = () => {
  /**
   * 1 phút KIM PHÚT quay được 360/60 = 6 độ
   * 1 giờ KIM GIỜ quay được 360/12 = 30 độ 
   * 1 phút KIM GIỜ quay được 30/60 = 0.5 độ 
   * góc lệch A = |KIM PHÚT - KIM GIỜ| = |(6 * số phút) - 0.5 * (60 * số giờ + số phút)|
   */

  let soGio = document.getElementById("soGio").value * 1;
  let soPhut = document.getElementById("soPhut").value * 1;
  let A; //góc lệch giữa kim giờ và kim phút;
  let result;

  A = Math.abs(6 * soPhut - 0.5 * (60 * soGio + soPhut)); 
  if (A > 180) {
    result = 360 - A;
  }
  else {
    result = A;
  }
  document.getElementById("result-c10").innerHTML = `Góc lệch giữa 2 kim là: ${result} độ`;
}
